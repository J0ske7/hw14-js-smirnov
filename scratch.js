function changeTheme() {
    const primaryColor = '#fff000';

    const isDefaultTheme = document.body.style.backgroundColor === '';

    if (isDefaultTheme) {
        document.body.style.backgroundColor = primaryColor;
        localStorage.setItem('theme', 'red');
    } else {
        document.body.style.backgroundColor = '';
        localStorage.setItem('theme', 'default');
    }
}
document.addEventListener('DOMContentLoaded', function () {
    const selectedTheme = localStorage.getItem('theme');
    if (selectedTheme === 'red') {
        document.body.style.backgroundColor = '#fff000';
    }
});